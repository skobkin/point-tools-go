package point_tools

// ApiError represents the typical error object in API response
type ApiError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
