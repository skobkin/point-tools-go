package point_tools

type SendAllPageResponse struct {
	Status string                  `json:"status"`
	Data   SendAllPageResponseData `json:"data"`
	Error  ApiError                `json:"error"`
}

type SendAllPageResponseData struct {
	Continue bool `json:"continue"`
}
