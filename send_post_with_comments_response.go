package point_tools

type SendPostWithCommentsResponse struct {
	Status string                           `json:"status"`
	Data   SendPostWithCommentsResponseData `json:"data"`
	Error  ApiError                         `json:"error"`
}

type SendPostWithCommentsResponseData struct {
	Continue bool   `json:"continue"`
	Next     string `json:"next"`
}
